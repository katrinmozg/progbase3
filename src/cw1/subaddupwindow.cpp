#include "subaddupwindow.h"
#include "ui_subaddupwindow.h"


SubAddUpWindow::SubAddUpWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SubAddUpWindow)
{
    ui->setupUi(this);
}

SubAddUpWindow::~SubAddUpWindow()
{
    delete ui;
}
Era SubAddUpWindow::data()
{
    Era e;
    e.era = ui->nameEdit->text().toStdString();
    e.time_period = ui->typeEdit->text().toStdString();
    return e;
}
void SubAddUpWindow::setTextToLabels(Era & era, QString window_name)
{
    this->name = window_name;
    if(window_name == "Update Window")
    {
        ui->nameEdit->setText(QString::fromStdString(era.era));
        ui->typeEdit->setText(QString::fromStdString(era.time_period));
    }

}
////
void SubAddUpWindow::fillAddedListWidget(Era era)
{
    era_ = era;
    vector<Dino> dinos = this->storage_->getAllEraDinos(era.id);
    this->added_dinos = dinos;
    for (Dino & dino : dinos)
    {
        QVariant qVariant = QVariant::fromValue(dino);

        QListWidgetItem *qDinoListItem = new QListWidgetItem();
        qDinoListItem->setText(QString::fromStdString(dino.name));
        qDinoListItem->setData(Qt::UserRole, qVariant);
        ui->addedListWidget->addItem(qDinoListItem);
    }

}
void SubAddUpWindow::fillAllListWidget()
{
    vector<Dino> dinos = this->storage_->getAllUserDinos(user_.id);
    this->all_dinos = dinos;
    for (Dino & dino : this->all_dinos)
    {
        bool flag = true;
        for(Dino &d : this->added_dinos)
        {
            if(dino.id == d.id)
            {
                flag = false;
                break;
            }
        }
            if(flag == true)
            {
                QVariant qVariant = QVariant::fromValue(dino);

                QListWidgetItem *qEraListItem = new QListWidgetItem();
                qEraListItem->setText(QString::fromStdString(dino.name));
                qEraListItem->setData(Qt::UserRole, qVariant);
                ui->allListWidget->addItem(qEraListItem);
            }
    }
}
void SubAddUpWindow::setConnection(Storage * storage, User user)
{
    this->user_ = user;
    this->storage_ = storage;
}
//

void SubAddUpWindow::on_allListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    Dino dino = var.value<Dino>();
    //
    QList <QListWidgetItem*> items = ui->allListWidget->selectedItems();
    QListWidgetItem* selectedItem = items.at(0);
    int row_index = ui->allListWidget->row(selectedItem);
    //take selected era
    int idx = 0;
    for(Dino d : this->all_dinos)
    {
        if(dino.id == d.id)
        {
            break;
        }
        idx++;
    }
    this->all_dinos.erase(this->all_dinos.begin() + idx);
    ui->allListWidget->takeItem((row_index));
    //delete from all eras
    bool isExist = false;
    for(size_t i = 0; i < added_dinos.size(); i++)
    {
       if(added_dinos.at(i).id == dino.id)
       {isExist = true;}
    }//check era in vector
    if(isExist == false)
    {added_dinos.push_back(dino);}
    //
    QListWidgetItem *qEraListItem = new QListWidgetItem();
    qEraListItem->setText(QString::fromStdString(dino.name));
    qEraListItem->setData(Qt::UserRole, var);
    ui->addedListWidget->addItem(qEraListItem);
    //add to added eras
    if(name == "Update Window")
    {
        bool flag = false;
        for(Dino d : this->removed_dinos)
        {
            if(dino.id == d.id)
            {
                flag = true;
                break;
            }
            idx++;
        }
        if(flag)
        this->removed_dinos.erase(this->removed_dinos.begin() + idx);

    }
}



void SubAddUpWindow::on_addedListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    Dino dino = var.value<Dino>();
    //
    QList <QListWidgetItem*> items = ui->addedListWidget->selectedItems();
    QListWidgetItem* selectedItem = items.at(0);
    int row_index = ui->addedListWidget->row(selectedItem);
    //take selected era
    int idx = 0;
    for(Dino d : this->added_dinos)
    {
        if(dino.id == d.id)
        {
            break;
        }
        idx++;
    }
    this->added_dinos.erase(this->added_dinos.begin() + idx);
    ui->addedListWidget->takeItem((row_index));
    //delete from all eras
    all_dinos.push_back(dino);
    //QVariant qVariant = QVariant::fromValue(era);
    QListWidgetItem *qDinoListItem = new QListWidgetItem();
    qDinoListItem->setText(QString::fromStdString(dino.name));
    qDinoListItem->setData(Qt::UserRole, var);
    ui->allListWidget->addItem(qDinoListItem);
    //add to added eras
    if(name == "Update Window")
    {
        this->removed_dinos.push_back(dino);
    }
}
