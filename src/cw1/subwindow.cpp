#include "subwindow.h"
#include "ui_subwindow.h"
#include "subaddupwindow.h"
#include "sublinksview.h"
#include <string>
#include "QVariant"
#include <QDebug>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QObject>
#include <vector>

//Q_DECLARE_METATYPE(Era);

SubWindow::SubWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SubWindow)
{
    ui->setupUi(this);
}

SubWindow::~SubWindow()
{
    delete ui;
}

void SubWindow::on_back_to_Dino_clicked()
{
    this->close();
}
//
////
void SubWindow::on_add_but_clicked()
{
    Era e;
    SubAddUpWindow addDialog(this);
    addDialog.setWindowTitle("Add Window");
    addDialog.setConnection(storage_, user);
    addDialog.fillAllListWidget();
    addDialog.setTextToLabels(e, "Add Window");
    int status = addDialog.exec();
    //qDebug() << status;

    if(status == 1)
    {
        e = addDialog.data();
        e.user_id = user.id;
        e.id = storage_->insertEra(e);
        addEra(e);
        //
        for(Dino d :addDialog.added_dinos)
        {
            storage_->insertDinoEra(d.id,e.id);
        }

    }
}

void SubWindow::on_up_but_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    SubAddUpWindow addDialog(this);
    addDialog.setWindowTitle("Update Window");

    Era era = items.at(0)->data(Qt::UserRole).value<Era>();
    addDialog.setTextToLabels(era, "Update Window");
    addDialog.setConnection(storage_, user);

    addDialog.fillAddedListWidget(era);
    addDialog.fillAllListWidget();

    if(addDialog.exec() == 1)
    {
        //auto d = addDialog.data();
        QListWidgetItem * selected_item = items.at(0);
        int id = selected_item->data(Qt::UserRole).value<Era>().id;
        Era e = addDialog.data();
        e.id = id;
        e.user_id = user.id;
        bool flag = storage_->updateEra(e);
        for(Dino d : addDialog.removed_dinos)
        {
            this->storage_->removeDinoEra(d.id, e.id);
        }
        vector<Dino> dinos = storage_->getAllEraDinos(e.id);

        for(Dino d : addDialog.added_dinos)
        {
            bool isExist = false;
            for(Dino dino : dinos)
            {
                if(dino.id == d.id)
                    isExist = true;
            }
            if(isExist == false)
            {
                this->storage_->insertDinoEra(d.id, e.id);
            }
        }
        qDebug()<<flag;
        if(flag)
        {
            QVariant var = QVariant::fromValue(e);
            selected_item->setText(QString::fromStdString(e.era));
            selected_item->setData(Qt::UserRole, var);

            fillInfoLabel(e);
        }

    }
}
//
void SubWindow::on_rem_but_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"On exit", "Are you sure?");
    if(reply == QMessageBox::StandardButton::Yes)
    {
        QList <QListWidgetItem*> items = ui->listWidget->selectedItems();
        if(items.count() == 0)
        {
            qDebug() << "No item selected";
        }
        else
        {
            QListWidgetItem* selectedItem = items.at(0);
            int id = selectedItem->data(Qt::UserRole).value<Era>().id;
            int row_index = ui->listWidget->row(selectedItem);
            ui->listWidget->takeItem((row_index));
            storage_->removeEra(id);
            delete selectedItem;

            if(ui->listWidget->count() == 0)
            {
                qDebug() << "last item was already removed.";
                ui->groupBox->hide();
                ui->up_but->setEnabled(false);
                ui->rem_but->setEnabled(false);
                return;
            }
            QListWidgetItem *new_selected_item =  ui->listWidget->selectedItems().at(0);
            Era era = new_selected_item->data(Qt::UserRole).value<Era>();

            enableDecor();

        }
    }
    else
    {
        qDebug()<<"do not remove";
    }

}

//
void SubWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    ui->up_but->setEnabled(true);
    ui->rem_but->setEnabled(true);

    QVariant var = item->data(Qt::UserRole);
    Era era = var.value<Era>();

    fillInfoLabel(era);
    ui->groupBox->setEnabled(true);
}
////

////
void SubWindow::addEra(const Era & e)
{
    QVariant var = QVariant::fromValue(e);

    QListWidgetItem *qEraListItem = new QListWidgetItem();
    qEraListItem->setText(QString::fromStdString(e.era));
    qEraListItem->setData(Qt::UserRole, var);

    ui->listWidget->addItem(qEraListItem);
}
//
void SubWindow::addErasToListWidget()
{
    ui->listWidget->clear();
    vector<Era> eras = this->storage_->getAllUserEras(user.id );
    for(const Era &e: eras)
    {
        addEra(e);
    }
}
//
void SubWindow::enableDecor()
{

        ui->add_but->setEnabled(true);
        ui->rem_but->setEnabled(false);
        ui->up_but->setEnabled(false);
        ui->listWidget->setEnabled(true);
        ui->groupBox->setEnabled(false);
}
//
void SubWindow::fillInfoLabel(Era &e)
{
    ui->era->setText(QString::fromStdString(e.era));
    ui->time_period->setText(QString::fromStdString(e.time_period));
}
////

void SubWindow::setLoginData(User userData, Storage* storage)
{
      storage_= storage;
      user = userData;
      enableDecor();
      addErasToListWidget();
}
////
void SubWindow::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    Era era = var.value<Era>();
    vector <Dino> dinos = this->storage_->getAllEraDinos(era.id);
    SubLinksView addDialog(this);
    addDialog.data(dinos);
    addDialog.exec();

}
////////////////////////////

