#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include "storage.h"
#include "authwindow.h"
#include "ui_authwindow.h"
#include "staticlib.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    bool openAgain = false;
signals:
    void toLogOut();
private slots:
    void beforeClose();
    void onLogOut();
    void toImport();
    void toExport();
    void aboutSaving();

    void on_add_but_clicked();

    void on_up_but_clicked();

    void on_rem_but_clicked();

    void on_era_menu_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void setLoginData(User userData, SqliteStorage* storage);

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::MainWindow *ui;
    Storage * storage_;
    bool isStorageOpen = false;
    User user;
    void addDino(const Dino & d);
    void addDinosToListWidget();
    void enableDecor();
    void fillInfoLabel(Dino &d);

};

#endif // MAINWINDOW_H
