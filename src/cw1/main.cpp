#include "mainwindow.h"
#include "authwindow.h"
#include "opendialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    AuthWindow auth;
    OpenDialog open;
    QObject::connect(&auth, SIGNAL(login(User, SqliteStorage*)), &w, SLOT(setLoginData(User, SqliteStorage*)));
    QObject::connect(&auth, SIGNAL(login(User, SqliteStorage*)), &w, SLOT(show()));
    QObject::connect(&open, SIGNAL(set_action(QString)), &auth, SLOT(getData(QString)));
    QObject::connect(&open, SIGNAL(set_action(QString)), &auth, SLOT(show()));
    QObject::connect(&w, SIGNAL(toLogOut()), &open, SLOT(show()));
    QObject::connect(&auth, SIGNAL(back()), &open, SLOT(show()));

    open.show();

    return a.exec();
}
