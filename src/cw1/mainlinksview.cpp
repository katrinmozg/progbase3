#include "mainlinksview.h"
#include "ui_mainlinksview.h"

MainLinksView::MainLinksView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainLinksView)
{
    ui->setupUi(this);
}

MainLinksView::~MainLinksView()
{
    delete ui;
}

void MainLinksView::data(vector<Era> & eras)
{

    for(Era e : eras)
    {
        QVariant var = QVariant::fromValue(e);

        QListWidgetItem *qEraListItem = new QListWidgetItem();
        qEraListItem->setText(QString::fromStdString(e.era));
        qEraListItem->setData(Qt::UserRole, var);

        ui->listWidget->addItem(qEraListItem);
    }
}

void MainLinksView::on_pushButton_clicked()
{
    this->close();
}
