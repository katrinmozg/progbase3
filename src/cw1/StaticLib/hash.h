#ifndef HASH_H
#define HASH_H

#include <QCryptographicHash>
#include <QDebug>
#include <QString>

namespace Hash
{
    QString hashPassword(QString const & pass);
}

#endif // HASH_H
