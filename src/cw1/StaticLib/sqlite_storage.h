#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#pragma once

#include <vector>
#include <string>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>

#include "hash.h"
#include "storage.h"
#include "optional.h"
#include "entity.h"

using std::string;
using std::vector;


class SqliteStorage : public Storage
{
    const string dir_name_;
    QSqlDatabase db_;

    Dino GetDinoFromQuery(const QSqlQuery & query);
    Era GetEraFromQuery(const QSqlQuery & query);

public:
    SqliteStorage(const string & dir_name);

    bool open();
    bool close();
    // dinosaurs
    vector<Dino> getAllDinosaurs(void);
    optional<Dino> getDinoById(int dino_id);
    bool updateDino(const Dino &dino);
    bool removeDino(int dino_id);
    int insertDino(const Dino &dino);
    // eras
    vector<Era> getAllEras(void);
    optional<Era> getEraById(int era_id);
    bool updateEra(const Era &era);
    bool removeEra(int era_id);
    int insertEra(const Era &era);
    //user
    optional<User> getUserAuth(string & username, string & password);
    int insertUser(const User &user);
    vector<Dino> getAllUserDinos(int user_id);
    vector<Era> getAllUserEras (int user_id);
    //links
    vector<Era> getAllDinoEras(int dino_id);
    vector<Dino> getAllEraDinos(int era_id);
    bool insertDinoEra(int student_id, int course_id);
    bool removeDinoEra(int student_id, int course_id);
};

#endif // SQLITE_STORAGE_H
