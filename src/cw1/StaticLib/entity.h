#pragma once
#include <string>
#include <QMetaType>
using std::string;


struct Dino 
{
    int id;
    std::string type;
    std::string name;
    std::string eater;
    std::string gun;
    int weight;
    int user_id;
};
Q_DECLARE_METATYPE(Dino);
struct Era 
{
    int id;
    std::string era;
    std::string time_period;
    int user_id;
};
Q_DECLARE_METATYPE(Era);
struct User
{
    int id;
    std::string login;
    std::string password_hash;
};

