#include "xmlstorage.h"
#include "entity.h"
#include <QtXml>
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <fstream>
using namespace std;

////
static QString readTextFromFile(QString & filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly))
    {
        qDebug() << "Can`t open file:" << filename << endl;
    }
    //read text from file
    QTextStream ts(&file);
    QString xml_text = ts.readAll();
    file.close();
    return xml_text;
}
static void writeTextToFile(QString & filename, QString & xml_text)
{
    QFile file(filename);
    if(!file.open(QFile::WriteOnly))
    {
        qDebug() << "Can`t open file to write:" << filename << endl;
    }
    //
    QTextStream ts(&file);
    ts << xml_text;
    file.close();
}
////

////
Dino XmlStorage::xmlElementToDino(const QDomNode & node)
{
    Dino d;
    QDomElement element = node.toElement();
    d.id = element.attribute("id").toInt();
    d.type = element.attribute("type").toStdString();
    d.name = element.attribute("name").toStdString();
    d.eater = element.attribute("eater").toStdString();
    d.weight = element.attribute("weight").toInt();
    d.gun = element.attribute("gun").toStdString();
    return d;
}
QDomElement XmlStorage::dinoToXmlElement(const Dino & d, QDomDocument * doc)
{
    QDomElement d_el = doc->createElement("dino");
    d_el.setAttribute("id", d.id);
    d_el.setAttribute("type", QString::fromStdString(d.type));
    d_el.setAttribute("name", QString::fromStdString(d.name));
    d_el.setAttribute("eater", QString::fromStdString(d.eater));
    d_el.setAttribute("weight", d.weight);
    d_el.setAttribute("gun", QString::fromStdString(d.gun));
    return d_el;
}
////

////
Era XmlStorage::xmlElementToEra(const QDomNode & node)
{
    Era e;
    QDomElement element = node.toElement();
    e.id = element.attribute("id").toInt();
    e.era = element.attribute("era").toStdString();
    e.time_period = element.attribute("time_period").toStdString();
    return e;
}
QDomElement XmlStorage::eraToXmlElement(const Era & e, QDomDocument * doc)
{
    QDomElement e_el = doc->createElement("era");
    e_el.setAttribute("id", e.id);
    e_el.setAttribute("era", QString::fromStdString(e.era));
    e_el.setAttribute("time_period", QString::fromStdString(e.time_period));
    return e_el;
}
////

////
bool XmlStorage::open()
{
    //dino
    //openfile
    QString dinosaurs_filename = QString::fromStdString(this->dir_name_ + "/dino.xml");
    QString xml_text = readTextFromFile(dinosaurs_filename);

    //parse text to domtree
    QDomDocument doc;
    QString errMsg;
    int errLine;
    int errCol;
    if(!doc.setContent(xml_text, &errMsg, &errLine, &errCol))
    {
        qDebug() << "Can`t parse xml text" << errMsg << errLine << errCol << endl;
        return false;
    }
    //parse elements of tree to table
    QDomElement root_el = doc.documentElement();
    xmlElementToDino(root_el);
    QDomNodeList children = root_el.childNodes();
    for(int i = 0; i < children.size(); i++)
    {
        QDomNode node = children.at(i);
        if(node.isElement())
        {
            Dino d = xmlElementToDino(node);
            this->dinosaurs_.push_back(d);
        }
    }

    //era
    QString eras_filename = QString::fromStdString(this->dir_name_ + "/era.xml");
    QString xml_text2 = readTextFromFile(eras_filename);

    //parse text to domtree
    QDomDocument doc2;
    if(!doc2.setContent(xml_text2, &errMsg, &errLine, &errCol))
    {
        qDebug() << "Can`t parse xml text" << errMsg << errLine << errCol << endl;
        return false;
    }
    //parse elements of tree to table
    QDomElement root_el2 = doc2.documentElement();
    xmlElementToDino(root_el2);
    QDomNodeList children2 = root_el2.childNodes();
    for(int i = 0; i < children2.size(); i++)
    {
        QDomNode node2 = children2.at(i);
        if(node2.isElement())
        {
            Era e = xmlElementToEra(node2);
            this->eras_.push_back(e);
        }
    }
    return true;
}
//
bool XmlStorage::close()
{
    //dino
    //create tree
    QDomDocument doc;
    QDomElement root_el = doc.createElement("dinosaurs");
    for(const Dino & d: this->dinosaurs_)
    {
        QDomElement d_el = dinoToXmlElement(d, &doc);
        root_el.appendChild(d_el);
    }
    doc.appendChild(root_el);
    //write to file
    QString dinosaurs_filename = QString::fromStdString(this->dir_name_ + "/dino.xml");
    QString xml_text = doc.toString(4);
    writeTextToFile(dinosaurs_filename, xml_text);

    //era
    QDomDocument doc2;
    QDomElement root_el2 = doc2.createElement("eras");
    for(const Era & e: this->eras_)
    {
        QDomElement e_el = eraToXmlElement(e, &doc2);
        root_el2.appendChild(e_el);
    }
    doc2.appendChild(root_el2);

    QString eras_filename = QString::fromStdString(this->dir_name_ + "/era.xml");
    QString xml_text2 = doc2.toString(4);
    writeTextToFile(eras_filename, xml_text2);

    return true;
}
////

vector<Dino> XmlStorage::getAllDinosaurs(void)
{
    return this->dinosaurs_;
}
//
optional<Dino> XmlStorage::getDinoById(int dino_id)
{
    for(const Dino & d: dinosaurs_)
    {
        if(d.id == dino_id)
        {
            return d;
        }
    }
    return nullopt;
}
//
bool XmlStorage::updateDino(const Dino &dino)
{
    auto dinos = getAllDinosaurs();
    for (int i = 0; i < (signed)dinos.size(); i++)
    {
        if (dinos.at(i).id == dino.id)
        {
            this->dinosaurs_.at(i).name = dino.name;
            this->dinosaurs_.at(i).type = dino.type;
            this->dinosaurs_.at(i).eater = dino.eater;
            this->dinosaurs_.at(i).weight = dino.weight;
            return true;
        }
    }

    return false;
}
//
bool XmlStorage::removeDino(int dino_id)
{
    int index = -1;
    for(int i = 0; i < (signed)dinosaurs_.size(); i ++)
    {
        if(dinosaurs_[i].id == dino_id)
        {
            index = i;
            break;
        }
    }
    if(index == -1)
    {
        return false;
    }
    dinosaurs_.erase(dinosaurs_.begin() + index);
    return true;
}
//
int XmlStorage::insertDino(const Dino &dino)
{
    int new_id = this->getNewDinoId();
    Dino copy = dino;
    copy.id = new_id;
    this->dinosaurs_.push_back(copy);
    return new_id;
}
//
int XmlStorage::getNewDinoId()
{
    int max_id = 0;
    for(const Dino & d: dinosaurs_)
    {
        if(d.id > max_id)
        {
            max_id = d.id;
        }
    }
    return max_id + 1;
}
int XmlStorage::getNewEraId()
{
    int max_id = 0;
    for(const Era & e: eras_)
    {
        if(e.id > max_id)
        {
            max_id = e.id;
        }
    }
    return max_id + 1;
}
////

////
vector<Era> XmlStorage::getAllEras(void)
{
    return this->eras_;
}
//
optional<Era> XmlStorage::getEraById(int era_id)
{
    for(const Era & e: eras_)
    {
        if(e.id == era_id)
        {
            return e;
        }
    }
    return nullopt;
}
//
bool XmlStorage::updateEra(const Era &era)
{
    auto eras = getAllEras();
    for (int i = 0; i < (signed)eras.size(); i++)
    {
        if (eras.at(i).id == era.id)
        {
            this->eras_.at(i).era = era.era;
            this->eras_.at(i).time_period = era.time_period;
            return true;
        }
    }
    return false;
}

bool XmlStorage::removeEra(int era_id)
{
    int index = -1;
    for(int i = 0; i < (signed)eras_.size(); i ++)
    {
        if(eras_[i].id == era_id)
        {
            index = i;
            break;
        }
    }
    if(index == -1)
    {
        return false;
    }
    eras_.erase(eras_.begin() + index);
    return true;
}
int XmlStorage::insertEra(const Era &era)
{
    int new_id = this->getNewEraId();
    Era copy = era;
    copy.id = new_id;
    this->eras_.push_back(copy);
    return new_id;
}
//user
optional<User> XmlStorage::getUserAuth(string & username, string & password)
{
    optional<User> user;
    return user;
}
int XmlStorage::insertUser(const User &user)
{
    return 1;
}
vector<Dino> XmlStorage::getAllUserDinos(int user_id)
{
    vector<Dino> dinos;
    return dinos;
}
vector<Era> XmlStorage::getAllUserEras (int user_id)
{
    vector<Era> eras;
    return eras;
}
//links
vector<Era> XmlStorage::getAllDinoEras(int dino_id)
{
    vector<Era> eras;
    return eras;
}
vector<Dino> XmlStorage::getAllEraDinos(int era_id)
{
    vector<Dino> dinos;
    return dinos;
}
bool XmlStorage::insertDinoEra(int student_id, int course_id)
{
    return false;
}
bool XmlStorage::removeDinoEra(int student_id, int course_id)
{
    return false;
}
