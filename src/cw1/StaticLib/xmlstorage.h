#pragma once

#include <vector>
#include <string>

#include "storage.h"
#include "optional.h"
#include "entity.h"
#include <QtXml>

using std::string;
using std::vector;

class XmlStorage : public Storage
{
    const string dir_name_;

    vector<Dino> dinosaurs_; // UPD for future (8.5.2019): add methods:
    vector<Era> eras_;

    static Dino xmlElementToDino(const QDomNode & node);
    static QDomElement dinoToXmlElement(const Dino & d, QDomDocument * doc);

    static Era xmlElementToEra(const QDomNode & node);
    static QDomElement eraToXmlElement(const Era & e, QDomDocument * doc);

    int getNewDinoId();
    int getNewEraId();

public:
    XmlStorage(const string &dir_name) : dir_name_(dir_name) {}

    bool open();
    bool close();
    // dinosaurs
    vector<Dino> getAllDinosaurs(void);
    optional<Dino> getDinoById(int dino_id);
    bool updateDino(const Dino &dino);
    bool removeDino(int dino_id);
    int insertDino(const Dino &dino);
    // eras
    vector<Era> getAllEras(void);
    optional<Era> getEraById(int era_id);
    bool updateEra(const Era &era);
    bool removeEra(int era_id);
    int insertEra(const Era &era);
    //user
    optional<User> getUserAuth(string & username, string & password);
    int insertUser(const User &user);
    vector<Dino> getAllUserDinos(int user_id);
    vector<Era> getAllUserEras (int user_id);
    //links
    vector<Era> getAllDinoEras(int dino_id);
    vector<Dino> getAllEraDinos(int era_id);
    bool insertDinoEra(int student_id, int course_id);
    bool removeDinoEra(int student_id, int course_id);

};

