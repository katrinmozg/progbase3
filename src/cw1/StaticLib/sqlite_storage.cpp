#include "sqlite_storage.h"

Dino SqliteStorage::GetDinoFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string type = query.value("type").toString().toStdString();
    string name = query.value("name").toString().toStdString();
    string eater = query.value("eater").toString().toStdString();
    int weight = query.value("weight").toInt();
    Dino d;
    d.id = id;
    d.type = type;
    d.name = name;
    d.eater = eater;
    d.weight = weight;
    return d;
}
Era SqliteStorage::GetEraFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string era = query.value("era").toString().toStdString();
    string time_period = query.value("time_period").toString().toStdString();

    Era e;
    e.id = id;
    e.era = era;
    e.time_period = time_period;
    return e;
}

SqliteStorage::SqliteStorage(const string & dir_name) : dir_name_(dir_name)
{
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_) + "/data.sqlite";
    db_.setDatabaseName(path);    // set sqlite database file path
    bool connected = db_.open();  // open db connection
    if (!connected)
    {
      return false;
    }
      return true;

}
bool SqliteStorage::close()
{
    db_.close();
    return true;
}
// dinosaurs
vector<Dino> SqliteStorage::getAllDinosaurs(void)
{
    vector<Dino> dinos;
    QSqlQuery query("SELECT * FROM dinos");
    //int idName = query.record().indexOf("name");
    while (query.next())
    {
        Dino d = GetDinoFromQuery(query);
        dinos.push_back(d);
    }
    //
    return dinos;

}

optional<Dino> SqliteStorage::getDinoById(int dino_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM dinos WHERE id = :id");
    query.bindValue(":id", dino_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get dino error:" << query.lastError();
    }
    if (query.next()) {
       qDebug() << " found ";
       Dino d = GetDinoFromQuery(query);
       return d;
    } else {
       qDebug() << " not found ";
       return nullopt;
    }

}
bool SqliteStorage::updateDino(const Dino &dino)
{
    QSqlQuery query;
    query.prepare("UPDATE dinos SET type = :type, name = :name, eater = :eater, weight = :weight, gun = :gun  WHERE id = :id");
    query.bindValue(":type", QString::fromStdString(dino.type));
    query.bindValue(":name", QString::fromStdString(dino.name));
    query.bindValue(":eater", QString::fromStdString(dino.eater));
    query.bindValue(":weight", dino.weight);
    query.bindValue(":gun", QString::fromStdString(dino.gun));
    query.bindValue(":id", dino.id);
    if (!query.exec()){
        qDebug() << "updateDino error:" << query.lastError();
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
bool SqliteStorage::removeDino(int dino_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM dinos WHERE id = :id");
    query.bindValue(":id", dino_id);
    if (!query.exec()){
        qDebug() << "deleteDino error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    vector<Era> eras = getAllDinoEras(dino_id);
    for(Era e : eras)
    {
        removeDinoEra(dino_id, e.id);
    }
    return true;
}
int SqliteStorage::insertDino(const Dino &dino)
{
    QSqlQuery query;
    query.prepare("INSERT INTO dinos (type, name, eater, weight, gun, user_id) VALUES (:type, :name, :eater, :weight, :gun, :user_id)");
    query.bindValue(":type", QString::fromStdString(dino.type));
    query.bindValue(":name", QString::fromStdString(dino.name));
    query.bindValue(":eater", QString::fromStdString(dino.eater));
    query.bindValue(":weight", dino.weight);
    query.bindValue(":gun", QString::fromStdString(dino.gun));
    query.bindValue(":user_id", dino.user_id);
    if (!query.exec()){
        qDebug() << "addDino error:"
                 << query.lastError();
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}


// eras
vector<Era> SqliteStorage::getAllEras(void)
{
    vector<Era> eras;
    QSqlQuery query("SELECT * FROM eras");
    //int idName = query.record().indexOf("name");
    while (query.next())
    {
        Era d = GetEraFromQuery(query);
        eras.push_back(d);
    }
    //
    return eras;
}
optional<Era> SqliteStorage::getEraById(int era_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM eras WHERE id = :id");
    query.bindValue(":id", era_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get era error:" << query.lastError();
    }
    if (query.next()) {
       qDebug() << " found ";
       Era e = GetEraFromQuery(query);
       return e;
    } else {
       qDebug() << " not found ";
       return nullopt;
    }
}
bool SqliteStorage::updateEra(const Era &era)
{
    QSqlQuery query;
    query.prepare("UPDATE eras SET era = :era, time_period = :time_period WHERE id = :id");
    query.bindValue(":era", QString::fromStdString(era.era));
    query.bindValue(":time_period", QString::fromStdString(era.time_period));
    query.bindValue(":id", era.id);
    if (!query.exec()){
        qDebug() << "updateEra error:" << query.lastError();
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;

}
bool SqliteStorage::removeEra(int era_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM eras WHERE id = :id");
    query.bindValue(":id", era_id);
    if (!query.exec()){
        qDebug() << "deleteEra error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    vector<Dino> dinos = getAllEraDinos(era_id);
    for(Dino d : dinos)
    {
        removeDinoEra(era_id, d.id);
    }
    return true;
}
int SqliteStorage::insertEra(const Era &era)
{
    QSqlQuery query;
    query.prepare("INSERT INTO eras (era, time_period, user_id) VALUES (:era, :time_period, :user_id)");
    query.bindValue(":era", QString::fromStdString(era.era));
    query.bindValue(":time_period", QString::fromStdString(era.time_period));
    query.bindValue(":user_id", era.user_id);
    if (!query.exec()){
        qDebug() << "addEra error:"
                 << query.lastError();
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}
/////
optional<User> SqliteStorage::getUserAuth(string & username, string & password)
{
    QString passwordHash = Hash::hashPassword(QString::fromStdString(password));

    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE login = :login AND password_hash = :password_hash");
    query.bindValue(":login", QString::fromStdString(username));
    query.bindValue(":password_hash", passwordHash);
    if (!query.exec()){
        qDebug() << "get user auth error:"
                 << query.lastError();
        return nullopt;
    }
    if(query.next())
    {
        qDebug()<< "user returned";
        User user;
        user.id = (query.value("id").toInt());
        user.login = (query.value("login").toString().toStdString());
        user.password_hash = (query.value("password_hash").toString().toStdString());
        qDebug()<<"is ok";
        return user;
    }

    return nullopt;
}
int SqliteStorage::insertUser(const User &user)
{

    QSqlQuery query;
    query.prepare("INSERT INTO users (login, password_hash) VALUES (:login, :password_hash)");
    query.bindValue(":login", QString::fromStdString(user.login));
    query.bindValue(":password_hash", QString::fromStdString(user.password_hash));
    if (!query.exec()){
        qDebug() << "addUser error:"
                 << query.lastError();
        return -1;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}
//
vector<Dino> SqliteStorage::getAllUserDinos(int user_id)
{
    vector<Dino> dinos;
    QSqlQuery query;
    query.prepare("SELECT id, type, name, eater, weight, gun FROM dinos WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if (!query.exec()){
        qDebug() << "get all user eras error:"
                 << query.lastError();
        abort();
    }

    while(query.next())
    {
        Dino dino;
        dino.id = (query.value("id").toInt());
        dino.type = (query.value("type").toString().toStdString());
        dino.name = (query.value("name").toString().toStdString());
        dino.eater = (query.value("eater").toString().toStdString());
        dino.weight = (query.value("weight").toInt());
        dino.gun = (query.value("gun").toString().toStdString());
        dinos.push_back(dino);
    }
    return dinos;
}
//
vector<Era> SqliteStorage::getAllUserEras(int user_id)
{
    vector<Era> eras;
    QSqlQuery query;
    query.prepare("SELECT id, era, time_period FROM eras WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if (!query.exec()){
        qDebug() << "get all user eras error:"
                 << query.lastError();
        abort();
    }

    while(query.next())
    {
        Era era;
        era.id = (query.value("id").toInt());
        era.era = (query.value("era").toString().toStdString());
        era.time_period = (query.value("time_period").toString().toStdString());
        eras.push_back(era);
    }
    return eras;
}
////
vector<Era> SqliteStorage::getAllDinoEras(int dino_id)
{
    vector<Era> eras;
    QSqlQuery query;
    query.prepare("SELECT eras.id, eras.era, eras.time_period, eras.user_id FROM eras, links WHERE links.dino_id = :dino_id AND links.era_id = eras.id");
    query.bindValue(":dino_id", dino_id);
    if (!query.exec()){
        qDebug() << "get all dino eras error:"
                 << query.lastError();
        abort();
    }

    while(query.next())
    {
        Era era;
        era.id = (query.value("id").toInt());
        era.era = (query.value("era").toString().toStdString());
        era.time_period = (query.value("time_period").toString().toStdString());
        era.user_id = (query.value("user_id").toInt());
        eras.push_back(era);
    }
    return eras;
}
vector<Dino> SqliteStorage::getAllEraDinos(int era_id)
{
    vector<Dino> dinos;
    QSqlQuery query;
    query.prepare("SELECT dinos.id, dinos.type, dinos.name, dinos.eater, dinos.weight, dinos.gun, dinos.user_id FROM dinos, links WHERE links.era_id = :era_id AND links.dino_id = dinos.id");
    query.bindValue(":era_id", era_id);
    if (!query.exec()){
        qDebug() << "get all era dinos error:"
                 << query.lastError();
        abort();
    }

    while(query.next())
    {
        Dino dino;
        dino.id = (query.value("id").toInt());
        dino.type = (query.value("type").toString().toStdString());
        dino.name = (query.value("name").toString().toStdString());
        dino.eater = (query.value("eater").toString().toStdString());
        dino.weight = (query.value("weight").toInt());
        dino.gun = (query.value("gun").toString().toStdString());
        dinos.push_back(dino);
    }
    return dinos;
}
bool SqliteStorage::insertDinoEra(int dino_id, int era_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO links (dino_id, era_id) VALUES (:dino_id, :era_id)");
    query.bindValue(":dino_id", dino_id);
    query.bindValue(":era_id", era_id);
    if (!query.exec()){
        qDebug() << "incert dino era error:"
                 << query.lastError();
        return false;
    }
    return true;
}
bool SqliteStorage::removeDinoEra(int dino_id, int era_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE dino_id = :dino_id AND era_id = :era_id");
    query.bindValue(":dino_id", dino_id);
    query.bindValue(":era_id", era_id);
    if (!query.exec()){
        qDebug() << "remove dino era error:"
                 << query.lastError();
        return false;
    }
    return true;
}
