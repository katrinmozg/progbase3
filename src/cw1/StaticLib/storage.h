#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.h"
#include "entity.h"

using std::string;
using std::vector;

class Storage
{
public:
    virtual ~Storage() {}
    virtual bool open() = 0;
    virtual bool close() = 0;
    // dinosaurs
    virtual vector<Dino> getAllDinosaurs(void) = 0;
    virtual optional<Dino> getDinoById(int dino_id) = 0;
    virtual bool updateDino(const Dino &dino) = 0;
    virtual bool removeDino(int dino_id) = 0;
    virtual int insertDino(const Dino &dino) = 0;
    // eras
    virtual vector<Era> getAllEras(void) = 0;
    virtual optional<Era> getEraById(int era_id) = 0;
    virtual bool updateEra(const Era &era) = 0;
    virtual bool removeEra(int era_id) = 0;
    virtual int insertEra(const Era &era) = 0;
    //user
    virtual optional<User> getUserAuth(string & username, string & password) = 0;
    virtual int insertUser(const User &user) = 0;
    virtual vector<Dino> getAllUserDinos(int user_id) = 0;
    virtual vector<Era> getAllUserEras (int user_id) = 0;
    //links
    virtual vector<Era> getAllDinoEras(int dino_id) = 0;
    virtual vector<Dino> getAllEraDinos(int era_id) = 0;
    virtual bool insertDinoEra(int student_id, int course_id) = 0;
    virtual bool removeDinoEra(int student_id, int course_id) = 0;

};
