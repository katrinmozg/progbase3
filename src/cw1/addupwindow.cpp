#include "addupwindow.h"
#include "ui_addupwindow.h"
#include "storage.h"
#include <QFileDialog>
#include <QDebug>

AddUpWindow::AddUpWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddUpWindow)
{
    ui->setupUi(this);
}

AddUpWindow::~AddUpWindow()
{
    delete ui;
}

Dino AddUpWindow::data()
{
    //Dino d;
    dino_.name = ui->nameEdit->text().toStdString();
    dino_.type = ui->typeEdit->text().toStdString();
    dino_.eater = ui->eaterEdit->text().toStdString();
    dino_.weight = ui->weightEdit->text().toInt();
    dino_.gun = pixpath.toStdString();
    return dino_;
}
void AddUpWindow::setTextToLabels(Dino & dino, QString window_name)
{
    this->name = window_name;
    if(window_name == "Update Window")
    {
        ui->nameEdit->setText(QString::fromStdString(dino.name));
        ui->typeEdit->setText(QString::fromStdString(dino.type));
        ui->eaterEdit->setText(QString::fromStdString(dino.eater));
        ui->weightEdit->setValue(dino.weight);
        pixpath = QString::fromStdString(dino.gun);
    }

}
////
void AddUpWindow::fillAddedListWidget(Dino dino)
{
    dino_ = dino;
    vector<Era> eras = this->storage_->getAllDinoEras(dino.id);
    this->added_eras = eras;
    for (Era & era : eras)
    {
        QVariant qVariant = QVariant::fromValue(era);

        QListWidgetItem *qEraListItem = new QListWidgetItem();
        qEraListItem->setText(QString::fromStdString(era.era));
        qEraListItem->setData(Qt::UserRole, qVariant);
        ui->addedListWidget->addItem(qEraListItem);
    }

}
void AddUpWindow::fillAllListWidget()
{
    vector<Era> eras = this->storage_->getAllUserEras(user_.id);
    this->all_eras = eras;
    for (Era & era : this->all_eras)
    {
        bool flag = true;
        for(Era &e : this->added_eras)
        {
            if(era.id == e.id)
            {
                flag = false;
                break;
            }
        }
            if(flag == true)
            {
                QVariant qVariant = QVariant::fromValue(era);

                QListWidgetItem *qEraListItem = new QListWidgetItem();
                qEraListItem->setText(QString::fromStdString(era.era));
                qEraListItem->setData(Qt::UserRole, qVariant);
                ui->allListWidget->addItem(qEraListItem);
            }
    }
}
void AddUpWindow::setConnection(Storage * storage, User user)
{
    this->user_ = user;
    this->storage_ = storage;  
}
//

void AddUpWindow::on_allListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    Era era = var.value<Era>();
    //
    QList <QListWidgetItem*> items = ui->allListWidget->selectedItems();
    QListWidgetItem* selectedItem = items.at(0);
    int row_index = ui->allListWidget->row(selectedItem);
    //take selected era
    int idx = 0;
    for(Era e : this->all_eras)
    {
        if(era.id == e.id)
        {
            break;
        }
        idx++;
    }
    this->all_eras.erase(this->all_eras.begin() + idx);
    ui->allListWidget->takeItem((row_index));
    //delete from all eras
    bool isExist = false;
    for(size_t i = 0; i < added_eras.size(); i++)
    {
       if(added_eras.at(i).id == era.id)
       {isExist = true;}
    }//check era in vector
    if(isExist == false)
    {added_eras.push_back(era);}
    //
    QListWidgetItem *qEraListItem = new QListWidgetItem();
    qEraListItem->setText(QString::fromStdString(era.era));
    qEraListItem->setData(Qt::UserRole, var);
    ui->addedListWidget->addItem(qEraListItem);
    //add to added eras
    if(name == "Update Window")
    {
        bool flag = false;
        for(Era e : this->removed_eras)
        {
            if(era.id == e.id)
            {
                flag = true;
                break;
            }
            idx++;
        }
        if(flag)
        this->removed_eras.erase(this->removed_eras.begin() + idx);

    }
}



void AddUpWindow::on_addedListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    Era era = var.value<Era>();
    //
    QList <QListWidgetItem*> items = ui->addedListWidget->selectedItems();
    QListWidgetItem* selectedItem = items.at(0);
    int row_index = ui->addedListWidget->row(selectedItem);
    //take selected era
    int idx = 0;
    for(Era e : this->added_eras)
    {
        if(era.id == e.id)
        {
            break;
        }
        idx++;
    }
    this->added_eras.erase(this->added_eras.begin() + idx);
    ui->addedListWidget->takeItem((row_index));
    //delete from all eras
    all_eras.push_back(era);
    //QVariant qVariant = QVariant::fromValue(era);
    QListWidgetItem *qEraListItem = new QListWidgetItem();
    qEraListItem->setText(QString::fromStdString(era.era));
    qEraListItem->setData(Qt::UserRole, var);
    ui->allListWidget->addItem(qEraListItem);
    //add to added eras
    if(name == "Update Window")
    {
        this->removed_eras.push_back(era);
    }
}



void AddUpWindow::on_open_clicked()
{

       QString fileNameInSys = QFileDialog::getOpenFileName(
                   this,              // parent
                   "Dialog Caption",  // caption
                   "",                // directory to start with
                   "PNG (*.png);;All Files (*)");  // file name filter
       qDebug() << fileNameInSys;
       //QDir dir("/home/katrin/KPI/projects/progbase3/src/cw1/data/picture");
       int i = fileNameInSys.lastIndexOf("/");
       //qDebug() << i;
       QString buf = fileNameInSys;
       QString fileName = buf.remove(0,i);
//       qDebug() << fileName;
//       qDebug() << fileNameInSys;
       QString fileNameInPro = "/home/katrin/KPI/projects/progbase3/src/cw1/data/picture" + fileName;
       QFile file(fileNameInPro);
       //
       //buf = fileNameInPro;
       if(file.exists(fileNameInPro))
       {
           int i = 1;
           while(file.exists(fileNameInPro))
           {
               fileNameInPro.chop(4);
               fileNameInPro += QString::fromStdString(std::to_string(i));
               fileNameInPro += ".png";
           }
       }
       qDebug()<< fileNameInPro;


       this->pixpath = fileNameInPro;
       QFile::copy( fileNameInSys, fileNameInPro);

    //ui->gunEdit->setText(fileNameInPro);
}
