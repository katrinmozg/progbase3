#-------------------------------------------------
#
# Project created by QtCreator 2019-06-06T18:53:27
#
#-------------------------------------------------
QT       += core gui
QT += xml
QT += sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cw1
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

INCLUDEPATH += "/home/katrin/KPI/projects/progbase3/src/cw1/StaticLib"
LIBS += "/home/katrin/KPI/projects/progbase3/src/cw1/build-StaticLib-Desktop_Qt_5_12_2_GCC_64bit2-Debug/libStaticLib.a"

SOURCES += \
        addupwindow.cpp \
        authwindow.cpp \
        main.cpp \
        mainlinksview.cpp \
        mainwindow.cpp \
        opendialog.cpp \
        subaddupwindow.cpp \
        sublinksview.cpp \
        subwindow.cpp \

HEADERS += \
    StaticLib/staticlib.h \
        addupwindow.h \
        authwindow.h \
        mainlinksview.h \
        mainwindow.h \
        opendialog.h \
        subaddupwindow.h \
        sublinksview.h \
        subwindow.h \

FORMS += \
        addupwindow.ui \
        authwindow.ui \
        mainlinksview.ui \
        mainwindow.ui \
        opendialog.ui \
        subaddupwindow.ui \
        sublinksview.ui \
        subwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
SUBDIRS += \
    StaticLib/StaticLib.pro
DISTFILES += \
    data/csv/dino.csv \
    data/csv/era.csv \
    data/sql/data.sqbpro \
    data/sql/data.sqlite \
    data/xml/dino.xml \
    data/xml/era.xml
