#include "opendialog.h"
#include "ui_opendialog.h"
#include "mainwindow.h"

OpenDialog::OpenDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenDialog)
{
    ui->setupUi(this);

}

OpenDialog::~OpenDialog()
{
    delete ui;
}


void OpenDialog::on_log_but_clicked()
{
    set_action("Log in");
    this->close();
}

void OpenDialog::on_sig_but_clicked()
{
    set_action("Sign up");
    this->close();
}



