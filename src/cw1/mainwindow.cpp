#include "mainwindow.h"
#include "opendialog.h"
#include "ui_mainwindow.h"
#include "authwindow.h"
#include "ui_authwindow.h"
#include "addupwindow.h"
#include "ui_addupwindow.h"
#include "subwindow.h"
#include "mainlinksview.h"
#include "xmlstorage.h"

#include "storage.h"


#include <string>
#include <QDebug>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QObject>
#include <vector>

//Q_DECLARE_METATYPE(Dino);

////
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->exit, &QAction::triggered, this, &MainWindow::beforeClose);
    connect(ui->actionLog_out, &QAction::triggered, this, &MainWindow::onLogOut);
    connect(ui->importXml, &QAction::triggered, this, &MainWindow:: toImport);
    connect(ui->exportXml, &QAction::triggered, this, &MainWindow:: toExport);
    connect(ui->aboutSaving, &QAction::triggered, this, &MainWindow:: aboutSaving);
}

MainWindow::~MainWindow()
{
    delete ui;
}
////

////
void MainWindow::on_era_menu_clicked()
{
    SubWindow addDialog(this);
    addDialog.setLoginData(user, this->storage_);
    int status = addDialog.exec();
}
//
void MainWindow::onLogOut()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"On exit", "Are you sure?");
    if(reply == QMessageBox::StandardButton::Yes)
    {
        //storage_->close();
        OpenDialog  open;
        this->close();
        emit toLogOut();
    }
    else
    {
        qDebug()<<"do not exit";
    }
}
void MainWindow::beforeClose()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"On exit", "Are you sure?");
    if(reply == QMessageBox::StandardButton::Yes)
    {
        storage_->close();
        //this->openAgain = false;
        this->close();
    }
    else
    {
        qDebug()<<"do not exit";
    }
}
////

////
void MainWindow::on_add_but_clicked()
{
    Dino d;
    AddUpWindow addDialog(this);
    addDialog.setWindowTitle("Add Window");
    addDialog.setConnection(storage_, user);
    addDialog.fillAllListWidget();
    addDialog.setTextToLabels(d, "Add Window");
    int status = addDialog.exec();
    //qDebug() << status;

    if(status == 1)
    {
        d = addDialog.data();
        d.user_id = user.id;
        d.id = storage_->insertDino(d);
        addDino(d);
        //
        for(Era e :addDialog.added_eras)
        {
            storage_->insertDinoEra(d.id,e.id);
        }
    }
    else
    {
        QString pixpath = addDialog.pixpath;
        QFile::remove(pixpath);
    }
}
//
void MainWindow::on_up_but_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    AddUpWindow addDialog(this);
    addDialog.setWindowTitle("Update Window");

    Dino dino = items.at(0)->data(Qt::UserRole).value<Dino>();
    qDebug()<<dino.id;
    addDialog.setTextToLabels(dino, "Update Window");
    addDialog.setConnection(storage_, user);

    addDialog.fillAddedListWidget(dino);
    addDialog.fillAllListWidget();

    if(addDialog.exec() == 1)
    {
        //auto d = addDialog.data();
        QListWidgetItem * selected_item = items.at(0);
        int id = selected_item->data(Qt::UserRole).value<Dino>().id;
        Dino d = addDialog.data();
        d.id = id;
        d.user_id = user.id;
        bool flag = storage_->updateDino(d);
        for(Era e : addDialog.removed_eras)
        {
            this->storage_->removeDinoEra(d.id, e.id);
        }
        vector<Era> eras = storage_->getAllDinoEras(d.id);

        for(Era e : addDialog.added_eras)
        {
            bool isExist = false;
            for(Era era : eras)
            {
                if(era.id == e.id)
                    isExist = true;
            }
            if(isExist == false)
            {
                this->storage_->insertDinoEra(d.id, e.id);
            }
        }
        qDebug()<<flag;
        if(flag)
        {
            QVariant var = QVariant::fromValue(d);
            selected_item->setText(QString::fromStdString(d.name));
            selected_item->setData(Qt::UserRole, var);

            fillInfoLabel(d);
        }

    }
}
//
void MainWindow::on_rem_but_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"On exit", "Are you sure?");
    if(reply == QMessageBox::StandardButton::Yes)
    {
        QList <QListWidgetItem*> items = ui->listWidget->selectedItems();
        if(items.count() == 0)
        {
            qDebug() << "No item selected";
        }
        else
        {
            QListWidgetItem* selectedItem = items.at(0);
            int id = selectedItem->data(Qt::UserRole).value<Dino>().id;
            int row_index = ui->listWidget->row(selectedItem);
            ui->listWidget->takeItem((row_index));
            storage_->removeDino(id);
            QString pixpath = QString::fromStdString(selectedItem->data(Qt::UserRole).value<Dino>().gun);
            QFile::remove(pixpath);
            delete selectedItem;

            if(ui->listWidget->count() == 0)
            {
                qDebug() << "last item was already removed.";
                ui->picture->clear();
                ui->groupBox->hide();
                ui->up_but->setEnabled(false);
                ui->rem_but->setEnabled(false);
                return;
            }
            QListWidgetItem *new_selected_item =  ui->listWidget->selectedItems().at(0);
            Dino dino = new_selected_item->data(Qt::UserRole).value<Dino>();

            enableDecor();


        }
    }

    else
    {
        qDebug()<<"do not remove";
    }

}
////

////
void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    ui->up_but->setEnabled(true);
    ui->rem_but->setEnabled(true);

    QVariant var = item->data(Qt::UserRole);
    Dino dino = var.value<Dino>();

    fillInfoLabel(dino);
    ui->groupBox->setEnabled(true);
    ui->groupBox->show();
}
//
void MainWindow::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    Dino dino = var.value<Dino>();
    vector <Era> eras = this->storage_->getAllDinoEras(dino.id);
    MainLinksView addDialog(this);
    addDialog.data(eras);
    addDialog.exec();

}
////

////
void MainWindow::addDino(const Dino & d)
{
    QVariant var = QVariant::fromValue(d);

    QListWidgetItem *qDinoListItem = new QListWidgetItem();
    qDinoListItem->setText(QString::fromStdString(d.name));
    qDinoListItem->setData(Qt::UserRole, var);

    ui->listWidget->addItem(qDinoListItem);
}
//
void MainWindow::addDinosToListWidget()
{
    ui->listWidget->clear();
    vector<Dino> dinos = this->storage_->getAllUserDinos(user.id );
    for(const Dino &d: dinos)
    {
        addDino(d);
    }
}
//
void MainWindow::enableDecor()
{
        ui->add_but->setEnabled(true);
        ui->rem_but->setEnabled(false);
        ui->up_but->setEnabled(false);
        ui->listWidget->setEnabled(true);
        ui->picture->clear();
        ui->name->clear();
        ui->type->clear();
        ui->eater->clear();
        ui->weight->clear();
        ui->groupBox->setEnabled(false);
}
//
void MainWindow::fillInfoLabel(Dino &d)
{
    ui->name->setText(QString::fromStdString(d.name));
    ui->type->setText(QString::fromStdString(d.type));
    ui->eater->setText(QString::fromStdString(d.eater));
    ui->weight->setText(QString::number(d.weight));
    QPixmap pix(QString::fromStdString(d.gun));
    int w = ui->picture->width();
    int h = ui->picture->height();
    ui->picture->setPixmap(pix);
    ui->picture->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));

    //


}
////

////
void MainWindow::setLoginData(User userData, SqliteStorage* storage)
{
      user = userData;
      this->storage_ = storage;
      enableDecor();
      addDinosToListWidget();
}
////

////
void MainWindow::toImport()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::information(this,"Import", "Choose dir with dino.xml, era.xml");
//  //
    QString dir_path = QFileDialog::getExistingDirectory(
                this,              // parent
                "Dialog Caption",  // caption
                "../cw1/data/",                // directory to start with
                QFileDialog::ShowDirsOnly
                | QFileDialog::DontResolveSymlinks);
    qDebug() << dir_path;

    if(dir_path.isEmpty())
    {
        qDebug() <<"can`t open dir";
        return;
    }
    XmlStorage *xml_storage = new XmlStorage(dir_path.toStdString());
    Storage * storage = xml_storage;
    storage->open();
//  //
    vector<Dino> old_dinos = storage_->getAllUserDinos(user.id);
    for(Dino d :old_dinos)
    {
        storage_->removeDino(d.id);
    }
    ui->listWidget->clear();
    //
    vector<Era> old_eras = storage_->getAllUserEras(user.id);
    for(Era e :old_eras)
    {
        storage_->removeEra(e.id);
    }
//  //
    bool status = false;
    vector<Dino> dinos = storage->getAllDinosaurs();
    for(Dino d : dinos)
    {
        d.user_id = user.id;
        status = storage_->insertDino(d);
        addDino(d);
    }
    if(status == false)
    {
        //todo Message
        qDebug()<< "Error in eras";
    }
    //
    vector<Era> eras = storage->getAllEras();
    for(Era e : eras)
    {
        e.user_id = user.id;
        status = storage_->insertEra(e);
    }
    if(status == false)
    {
        //todo Message
        qDebug()<< "Error in eras";
    }
    storage->close();
    delete storage;
}
////
void MainWindow::toExport()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::information(this,"Import", "Choose dir to save dino.xml, era.xml");
//  //

    QString dir_path = QFileDialog::getExistingDirectory(
                this,              // parent
                "Dialog Caption",  // caption
                "../cw1/data/",                // directory to start with
                QFileDialog::ShowDirsOnly
                | QFileDialog::DontResolveSymlinks);
    qDebug() << dir_path;

    if(dir_path.isEmpty())
    {
        qDebug() <<"can`t open dir";
        return;
    }
    QString default_name = "/xml_emport";
    QDir dir(dir_path + default_name);
        int i = 1;
        while(dir.exists(dir_path + default_name))
        {
            default_name = "/xml_emport" + QString::fromStdString(std::to_string(i));
        }
    dir_path += default_name;
    QDir().mkdir(dir_path);
    QFile dfile(dir_path + "dino.xml");
    QFile efile(dir_path + "era.xml");


    XmlStorage *xml_storage = new XmlStorage(dir_path.toStdString());
    Storage * storage = xml_storage;

    storage->open();

    vector<Dino> dinos = storage_->getAllUserDinos(user.id);
    for(Dino d : dinos)
    {
        storage->insertDino(d);
    }
    //
    vector<Era> eras = storage_->getAllUserEras(user.id);
    for(Era e : eras)
    {
        storage->insertEra(e);
    }
    //
    storage->close();
    delete storage;
}
////
void MainWindow::aboutSaving()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::information(this,"About Saving", "The data is automaticlly stored in sqlite data base.\nIf you wish, you can import data from xml, but the original data will be erased.\nAlso you can export your data to xml :)");
}
