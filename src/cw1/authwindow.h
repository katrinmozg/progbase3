#ifndef AUTHWINDOW_H
#define AUTHWINDOW_H

#include <string>
#include <QDebug>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QObject>
#include <vector>
#include "storage.h"
#include "sqlite_storage.h"
#include "staticlib.h"


namespace Ui {
class AuthWindow;
}

class AuthWindow : public QDialog
{
    Q_OBJECT
public:
    explicit AuthWindow(QWidget *parent = nullptr);
    ~AuthWindow();
    //void getData(QString action);
    QString action_;

private slots:
    void on_sign_in_clicked();
    void getData(QString action);

    void on_back_clicked();

signals:
    void login(User userData, SqliteStorage * storage);
    void back();

private:
    SqliteStorage * storage;
    //QLineEdit* loginEdit;
    //QLineEdit* passwordEdit;

    QPushButton* buttonSign;

    bool connected;
    //
    Ui::AuthWindow *ui;

};

#endif // AUTHWINDOW_H
