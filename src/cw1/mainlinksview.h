#ifndef MAINLINKSVIEW_H
#define MAINLINKSVIEW_H

#include <QDialog>
#include "entity.h"
#include <vector>
#include "staticlib.h"
using std::vector;

namespace Ui {
class MainLinksView;
}

class MainLinksView : public QDialog
{
    Q_OBJECT

public:
    explicit MainLinksView(QWidget *parent = nullptr);
    ~MainLinksView();

    void data(vector<Era> & eras);
private slots:
    void on_pushButton_clicked();

private:
    Ui::MainLinksView *ui;
};

#endif // MAINLINKSVIEW_H
