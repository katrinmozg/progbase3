#include "sublinksview.h"
#include "ui_sublinksview.h"

SubLinksView::SubLinksView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SubLinksView)
{
    ui->setupUi(this);
}

SubLinksView::~SubLinksView()
{
    delete ui;
}

void SubLinksView::data(vector<Dino> & dinos)
{

    for(Dino d : dinos)
    {
        QVariant var = QVariant::fromValue(d);

        QListWidgetItem *qDinoListItem = new QListWidgetItem();
        qDinoListItem->setText(QString::fromStdString(d.name));
        qDinoListItem->setData(Qt::UserRole, var);

        ui->listWidget->addItem(qDinoListItem);
    }
}

void SubLinksView::on_pushButton_clicked()
{
    this->close();
}
