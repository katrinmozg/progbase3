#ifndef SUBADDUPWINDOW_H
#define SUBADDUPWINDOW_H

#include <QDialog>
#include <entity.h>
#include "storage.h"
#include <QListWidgetItem>
#include <algorithm>
#include <vector>
#include "staticlib.h"

namespace Ui {
class SubAddUpWindow;
}

class SubAddUpWindow : public QDialog
{
    Q_OBJECT
public:
    explicit SubAddUpWindow(QWidget *parent = nullptr);
    ~SubAddUpWindow();
    QString name;
    User user_;
    Storage * storage_;
    Era era_;
    vector<Dino> all_dinos;
    vector<Dino> added_dinos;
    vector<Dino> removed_dinos;
    Era data();
    void setTextToLabels(Era & era, QString window_name);
    void setConnection(Storage * storage, User user);
    void fillAllListWidget();
    void fillAddedListWidget(Era era);
private slots:
    void on_allListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_addedListWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::SubAddUpWindow *ui;
};

#endif // SUBADDUPWINDOW_H
