#include "authwindow.h"
#include "ui_authwindow.h"
#include "mainwindow.h"
#include "staticlib.h"

AuthWindow::AuthWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthWindow)
{
    ui->setupUi(this);
    connected = false;
    storage =  new SqliteStorage("/home/katrin/KPI/projects/progbase3/src/cw1/data/sql");
    if(storage != nullptr)
    {
        connected = storage->open();
        qDebug()<<"open"<<connected;
    }
    else
    {
        QMessageBox::critical(nullptr,
                              QObject::tr("Error"),
                              QObject::tr("Out of memory."),
                              QMessageBox::Cancel);
    }

}

AuthWindow::~AuthWindow()
{
    //delete storage;
    delete ui;
}

void AuthWindow::on_sign_in_clicked()
{
    string username;
    string password;

    username = ui->loginEdit->text().toStdString();
    password = ui->passwordEdit->text().toStdString();

    if(action_ == "Log in")
    {
        optional<User> user_op = storage->getUserAuth(username, password);
        if(user_op)
        {
            User userData = user_op.value();
            login(userData, storage);

            this->close();
        }
        else
        {
            QMessageBox::information(nullptr, QObject::tr("Authorization"),
                                              QObject::tr("Incorrect username or password"),
                                              QMessageBox::Ok);
        }
    }
    if(action_ == "Sign up")
    {
        QString passwordHash = Hash::hashPassword(QString::fromStdString(password));
        User user;
        user.login = username;
        user.password_hash = passwordHash.toStdString();
        int id = storage->insertUser(user);
        qDebug()<<id << "id of new user";
        if(id != -1)
        {
            emit login(user, storage);
            this->close();
        }
        else
        {
            QMessageBox::information(nullptr, QObject::tr("Log in"),
                                              QObject::tr("This name is already used"),
                                              QMessageBox::Ok);
        }


    }
}

void AuthWindow::getData(QString action)
{
    ui->loginEdit->clear();
    ui->passwordEdit->clear();
    this->action_ = action;
}

void AuthWindow::on_back_clicked()
{
    this->close();
    emit back();
}
