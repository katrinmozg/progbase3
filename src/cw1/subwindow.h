#ifndef SUBWINDOW_H
#define SUBWINDOW_H

#include <QDialog>
#include <QMainWindow>
#include <QListWidget>
#include "storage.h"
#include "sqlite_storage.h"
#include "staticlib.h"

namespace Ui {
class SubWindow;
}

class SubWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SubWindow(QWidget *parent = nullptr);
    void setLoginData(User userData, Storage* storage);
    ~SubWindow();

private slots:

    void on_add_but_clicked();

    void on_up_but_clicked();

    void on_rem_but_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_back_to_Dino_clicked();

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::SubWindow *ui;
    Storage * storage_;
    bool isStorageOpen = false;
    User user;
    void addEra(const Era & e);
    void addErasToListWidget();
    void enableDecor();
    void fillInfoLabel(Era &e);
};

#endif // SUBWINDOW_H
