#ifndef SUBLINKSVIEW_H
#define SUBLINKSVIEW_H

#include <QDialog>
#include "entity.h"
#include <vector>
#include "staticlib.h"

using std::vector;

namespace Ui {
class SubLinksView;
}

class SubLinksView : public QDialog
{
    Q_OBJECT

public:
    explicit SubLinksView(QWidget *parent = nullptr);
    ~SubLinksView();
    void data(vector<Dino> & dinos);
private slots:
    void on_pushButton_clicked();

private:
    Ui::SubLinksView *ui;
};

#endif // SUBLINKSVIEW_H
