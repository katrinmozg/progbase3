#ifndef ADDUPWINDOW_H
#define ADDUPWINDOW_H

#include <QDialog>
#include <entity.h>
#include "storage.h"
#include <QListWidgetItem>
#include <algorithm>
#include "staticlib.h"


namespace Ui {
class AddUpWindow;
}

class AddUpWindow : public QDialog
{
    Q_OBJECT

public:
    explicit AddUpWindow(QWidget *parent = nullptr);
    ~AddUpWindow();
    QString name;
    User user_;
    Storage * storage_;
    Dino dino_;
    vector<Era> all_eras;
    vector<Era> added_eras;
    vector<Era> removed_eras;
    QString pixpath;
    Dino data();
    void setTextToLabels(Dino & dino, QString window_name);
    void setConnection(Storage * storage, User user);
    void fillAllListWidget();
    void fillAddedListWidget(Dino dino);
private slots:
    void on_allListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_addedListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_open_clicked();

private:
    Ui::AddUpWindow *ui;

};

#endif // ADDUPWINDOW_H
