#ifndef OPENDIALOG_H
#define OPENDIALOG_H

#include <QDialog>
#include <string.h>
#include "authwindow.h"


namespace Ui {
class OpenDialog;
}

class OpenDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OpenDialog(QWidget *parent = nullptr);
    ~OpenDialog();

signals:
    void set_action(QString action);

private slots:
    void on_log_but_clicked();

    void on_sig_but_clicked();

private:
    Ui::OpenDialog *ui;
};

#endif // OPENDIALOG_H
