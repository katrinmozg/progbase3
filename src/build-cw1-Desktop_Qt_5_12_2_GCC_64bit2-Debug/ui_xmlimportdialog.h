/********************************************************************************
** Form generated from reading UI file 'xmlimportdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_XMLIMPORTDIALOG_H
#define UI_XMLIMPORTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_XmlImportDialog
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *dinos;
    QPushButton *eras;
    QPushButton *links;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *labelDino;
    QLabel *labelEra;
    QLabel *labelLinks;
    QDialogButtonBox *buttonBox_2;

    void setupUi(QDialog *XmlImportDialog)
    {
        if (XmlImportDialog->objectName().isEmpty())
            XmlImportDialog->setObjectName(QString::fromUtf8("XmlImportDialog"));
        XmlImportDialog->resize(521, 274);
        verticalLayoutWidget = new QWidget(XmlImportDialog);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(20, 10, 151, 211));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(20);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        dinos = new QPushButton(verticalLayoutWidget);
        dinos->setObjectName(QString::fromUtf8("dinos"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        dinos->setFont(font);

        verticalLayout->addWidget(dinos);

        eras = new QPushButton(verticalLayoutWidget);
        eras->setObjectName(QString::fromUtf8("eras"));
        eras->setFont(font);

        verticalLayout->addWidget(eras);

        links = new QPushButton(verticalLayoutWidget);
        links->setObjectName(QString::fromUtf8("links"));
        links->setFont(font);

        verticalLayout->addWidget(links);

        verticalLayoutWidget_2 = new QWidget(XmlImportDialog);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(180, 10, 321, 211));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(10, 0, 0, 0);
        labelDino = new QLabel(verticalLayoutWidget_2);
        labelDino->setObjectName(QString::fromUtf8("labelDino"));
        labelDino->setFont(font);

        verticalLayout_2->addWidget(labelDino);

        labelEra = new QLabel(verticalLayoutWidget_2);
        labelEra->setObjectName(QString::fromUtf8("labelEra"));
        labelEra->setFont(font);

        verticalLayout_2->addWidget(labelEra);

        labelLinks = new QLabel(verticalLayoutWidget_2);
        labelLinks->setObjectName(QString::fromUtf8("labelLinks"));
        labelLinks->setFont(font);

        verticalLayout_2->addWidget(labelLinks);

        buttonBox_2 = new QDialogButtonBox(XmlImportDialog);
        buttonBox_2->setObjectName(QString::fromUtf8("buttonBox_2"));
        buttonBox_2->setGeometry(QRect(150, 230, 341, 32));
        buttonBox_2->setFont(font);
        buttonBox_2->setOrientation(Qt::Horizontal);
        buttonBox_2->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        retranslateUi(XmlImportDialog);

        QMetaObject::connectSlotsByName(XmlImportDialog);
    } // setupUi

    void retranslateUi(QDialog *XmlImportDialog)
    {
        XmlImportDialog->setWindowTitle(QApplication::translate("XmlImportDialog", "Dialog", nullptr));
        dinos->setText(QApplication::translate("XmlImportDialog", "Choose dinos.xml", nullptr));
        eras->setText(QApplication::translate("XmlImportDialog", "Choose eras.xml", nullptr));
        links->setText(QApplication::translate("XmlImportDialog", "Choose links.xml", nullptr));
        labelDino->setText(QApplication::translate("XmlImportDialog", "TextLabel", nullptr));
        labelEra->setText(QApplication::translate("XmlImportDialog", "TextLabel", nullptr));
        labelLinks->setText(QApplication::translate("XmlImportDialog", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class XmlImportDialog: public Ui_XmlImportDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_XMLIMPORTDIALOG_H
