/********************************************************************************
** Form generated from reading UI file 'mainlinksview.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINLINKSVIEW_H
#define UI_MAINLINKSVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_MainLinksView
{
public:
    QPushButton *pushButton;
    QListWidget *listWidget;

    void setupUi(QDialog *MainLinksView)
    {
        if (MainLinksView->objectName().isEmpty())
            MainLinksView->setObjectName(QString::fromUtf8("MainLinksView"));
        MainLinksView->resize(295, 337);
        pushButton = new QPushButton(MainLinksView);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(180, 300, 80, 25));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        pushButton->setFont(font);
        listWidget = new QListWidget(MainLinksView);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(20, 20, 256, 261));
        listWidget->setFont(font);

        retranslateUi(MainLinksView);

        QMetaObject::connectSlotsByName(MainLinksView);
    } // setupUi

    void retranslateUi(QDialog *MainLinksView)
    {
        MainLinksView->setWindowTitle(QApplication::translate("MainLinksView", "Dialog", nullptr));
        pushButton->setText(QApplication::translate("MainLinksView", "<=Back", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainLinksView: public Ui_MainLinksView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINLINKSVIEW_H
