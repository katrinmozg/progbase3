/********************************************************************************
** Form generated from reading UI file 'addupwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDUPWINDOW_H
#define UI_ADDUPWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddUpWindow
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_10;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *nameEdit;
    QLineEdit *typeEdit;
    QLineEdit *eaterEdit;
    QSpinBox *weightEdit;
    QPushButton *open;
    QListWidget *allListWidget;
    QLabel *label_5;
    QListWidget *addedListWidget;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;

    void setupUi(QDialog *AddUpWindow)
    {
        if (AddUpWindow->objectName().isEmpty())
            AddUpWindow->setObjectName(QString::fromUtf8("AddUpWindow"));
        AddUpWindow->resize(449, 632);
        buttonBox = new QDialogButtonBox(AddUpWindow);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(60, 580, 341, 32));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        verticalLayoutWidget = new QWidget(AddUpWindow);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(30, 30, 61, 251));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        verticalLayout->addWidget(label_2);

        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        verticalLayout->addWidget(label);

        label_4 = new QLabel(verticalLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        verticalLayout->addWidget(label_4);

        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);

        verticalLayout->addWidget(label_3);

        label_10 = new QLabel(verticalLayoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font);

        verticalLayout->addWidget(label_10);

        verticalLayoutWidget_2 = new QWidget(AddUpWindow);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(110, 30, 291, 251));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        nameEdit = new QLineEdit(verticalLayoutWidget_2);
        nameEdit->setObjectName(QString::fromUtf8("nameEdit"));
        nameEdit->setFont(font);

        verticalLayout_2->addWidget(nameEdit);

        typeEdit = new QLineEdit(verticalLayoutWidget_2);
        typeEdit->setObjectName(QString::fromUtf8("typeEdit"));
        typeEdit->setFont(font);

        verticalLayout_2->addWidget(typeEdit);

        eaterEdit = new QLineEdit(verticalLayoutWidget_2);
        eaterEdit->setObjectName(QString::fromUtf8("eaterEdit"));
        eaterEdit->setFont(font);

        verticalLayout_2->addWidget(eaterEdit);

        weightEdit = new QSpinBox(verticalLayoutWidget_2);
        weightEdit->setObjectName(QString::fromUtf8("weightEdit"));
        weightEdit->setFont(font);
        weightEdit->setMinimum(1);
        weightEdit->setMaximum(100000);
        weightEdit->setSingleStep(10);

        verticalLayout_2->addWidget(weightEdit);

        open = new QPushButton(verticalLayoutWidget_2);
        open->setObjectName(QString::fromUtf8("open"));
        open->setFont(font);

        verticalLayout_2->addWidget(open);

        allListWidget = new QListWidget(AddUpWindow);
        allListWidget->setObjectName(QString::fromUtf8("allListWidget"));
        allListWidget->setGeometry(QRect(20, 380, 171, 191));
        allListWidget->setFont(font);
        label_5 = new QLabel(AddUpWindow);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(30, 320, 371, 20));
        label_5->setFont(font);
        label_5->setAlignment(Qt::AlignCenter);
        addedListWidget = new QListWidget(AddUpWindow);
        addedListWidget->setObjectName(QString::fromUtf8("addedListWidget"));
        addedListWidget->setGeometry(QRect(250, 380, 171, 192));
        addedListWidget->setFont(font);
        label_6 = new QLabel(AddUpWindow);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(190, 440, 61, 31));
        label_6->setFont(font);
        label_6->setAlignment(Qt::AlignCenter);
        label_7 = new QLabel(AddUpWindow);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(190, 480, 61, 31));
        label_7->setFont(font);
        label_7->setAlignment(Qt::AlignCenter);
        label_8 = new QLabel(AddUpWindow);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setEnabled(false);
        label_8->setGeometry(QRect(240, 350, 191, 31));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Noto Sans"));
        font1.setPointSize(11);
        font1.setBold(true);
        font1.setWeight(75);
        label_8->setFont(font1);
        label_8->setAlignment(Qt::AlignCenter);
        label_9 = new QLabel(AddUpWindow);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setEnabled(false);
        label_9->setGeometry(QRect(10, 350, 191, 31));
        label_9->setFont(font1);
        label_9->setAlignment(Qt::AlignCenter);

        retranslateUi(AddUpWindow);
        QObject::connect(buttonBox, SIGNAL(accepted()), AddUpWindow, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AddUpWindow, SLOT(reject()));

        QMetaObject::connectSlotsByName(AddUpWindow);
    } // setupUi

    void retranslateUi(QDialog *AddUpWindow)
    {
        AddUpWindow->setWindowTitle(QApplication::translate("AddUpWindow", "Dialog", nullptr));
        label_2->setText(QApplication::translate("AddUpWindow", "Name", nullptr));
        label->setText(QApplication::translate("AddUpWindow", "Type", nullptr));
        label_4->setText(QApplication::translate("AddUpWindow", "Eater", nullptr));
        label_3->setText(QApplication::translate("AddUpWindow", "Weight", nullptr));
        label_10->setText(QApplication::translate("AddUpWindow", "Gun", nullptr));
        nameEdit->setPlaceholderText(QApplication::translate("AddUpWindow", "name", nullptr));
        typeEdit->setPlaceholderText(QApplication::translate("AddUpWindow", "type", nullptr));
        eaterEdit->setPlaceholderText(QApplication::translate("AddUpWindow", "eater", nullptr));
        open->setText(QApplication::translate("AddUpWindow", "Choose png file", nullptr));
        label_5->setText(QApplication::translate("AddUpWindow", "Chose connection with era", nullptr));
        label_6->setText(QApplication::translate("AddUpWindow", "====>", nullptr));
        label_7->setText(QApplication::translate("AddUpWindow", "<====", nullptr));
        label_8->setText(QApplication::translate("AddUpWindow", "~Double click to remove", nullptr));
        label_9->setText(QApplication::translate("AddUpWindow", "~Double click to add", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AddUpWindow: public Ui_AddUpWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDUPWINDOW_H
