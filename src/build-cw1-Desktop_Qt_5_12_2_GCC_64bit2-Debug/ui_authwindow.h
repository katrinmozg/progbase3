/********************************************************************************
** Form generated from reading UI file 'authwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTHWINDOW_H
#define UI_AUTHWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_AuthWindow
{
public:
    QLineEdit *loginEdit;
    QLineEdit *passwordEdit;
    QLabel *label;
    QLabel *label_2;
    QPushButton *sign_in;
    QPushButton *back;

    void setupUi(QDialog *AuthWindow)
    {
        if (AuthWindow->objectName().isEmpty())
            AuthWindow->setObjectName(QString::fromUtf8("AuthWindow"));
        AuthWindow->resize(400, 300);
        loginEdit = new QLineEdit(AuthWindow);
        loginEdit->setObjectName(QString::fromUtf8("loginEdit"));
        loginEdit->setGeometry(QRect(40, 100, 311, 25));
        passwordEdit = new QLineEdit(AuthWindow);
        passwordEdit->setObjectName(QString::fromUtf8("passwordEdit"));
        passwordEdit->setGeometry(QRect(40, 170, 311, 25));
        passwordEdit->setAcceptDrops(true);
        passwordEdit->setEchoMode(QLineEdit::Password);
        passwordEdit->setDragEnabled(false);
        passwordEdit->setReadOnly(false);
        label = new QLabel(AuthWindow);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(40, 60, 311, 41));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label_2 = new QLabel(AuthWindow);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(40, 129, 311, 41));
        label_2->setFont(font);
        sign_in = new QPushButton(AuthWindow);
        sign_in->setObjectName(QString::fromUtf8("sign_in"));
        sign_in->setGeometry(QRect(230, 230, 121, 31));
        sign_in->setFont(font);
        back = new QPushButton(AuthWindow);
        back->setObjectName(QString::fromUtf8("back"));
        back->setGeometry(QRect(40, 230, 121, 31));
        back->setFont(font);

        retranslateUi(AuthWindow);

        QMetaObject::connectSlotsByName(AuthWindow);
    } // setupUi

    void retranslateUi(QDialog *AuthWindow)
    {
        AuthWindow->setWindowTitle(QApplication::translate("AuthWindow", "Dialog", nullptr));
        label->setText(QApplication::translate("AuthWindow", "Login", nullptr));
        label_2->setText(QApplication::translate("AuthWindow", "Password", nullptr));
        sign_in->setText(QApplication::translate("AuthWindow", "Ok", nullptr));
        back->setText(QApplication::translate("AuthWindow", "<=Back", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AuthWindow: public Ui_AuthWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTHWINDOW_H
