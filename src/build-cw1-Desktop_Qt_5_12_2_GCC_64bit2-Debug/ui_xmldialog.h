/********************************************************************************
** Form generated from reading UI file 'xmldialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_XMLDIALOG_H
#define UI_XMLDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_XmlDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_3;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;

    void setupUi(QDialog *XmlDialog)
    {
        if (XmlDialog->objectName().isEmpty())
            XmlDialog->setObjectName(QString::fromUtf8("XmlDialog"));
        XmlDialog->resize(498, 300);
        buttonBox = new QDialogButtonBox(XmlDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(140, 260, 341, 32));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        verticalLayoutWidget = new QWidget(XmlDialog);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 40, 151, 211));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(20);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_3 = new QPushButton(verticalLayoutWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setFont(font);

        verticalLayout->addWidget(pushButton_3);

        pushButton_2 = new QPushButton(verticalLayoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setFont(font);

        verticalLayout->addWidget(pushButton_2);

        pushButton = new QPushButton(verticalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font);

        verticalLayout->addWidget(pushButton);

        verticalLayoutWidget_2 = new QWidget(XmlDialog);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(170, 40, 321, 211));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(10, 0, 0, 0);
        label = new QLabel(verticalLayoutWidget_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        verticalLayout_2->addWidget(label);

        label_2 = new QLabel(verticalLayoutWidget_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        verticalLayout_2->addWidget(label_2);

        label_3 = new QLabel(verticalLayoutWidget_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);

        verticalLayout_2->addWidget(label_3);


        retranslateUi(XmlDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), XmlDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), XmlDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(XmlDialog);
    } // setupUi

    void retranslateUi(QDialog *XmlDialog)
    {
        XmlDialog->setWindowTitle(QApplication::translate("XmlDialog", "Dialog", nullptr));
        pushButton_3->setText(QApplication::translate("XmlDialog", "Choose dinos.xml", nullptr));
        pushButton_2->setText(QApplication::translate("XmlDialog", "Choose eras.xml", nullptr));
        pushButton->setText(QApplication::translate("XmlDialog", "Choose links.xml", nullptr));
        label->setText(QApplication::translate("XmlDialog", "TextLabel", nullptr));
        label_2->setText(QApplication::translate("XmlDialog", "TextLabel", nullptr));
        label_3->setText(QApplication::translate("XmlDialog", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class XmlDialog: public Ui_XmlDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_XMLDIALOG_H
