/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *exit;
    QAction *actionImport_from_XML;
    QAction *actionExport_to_XML;
    QAction *actionLog_out;
    QAction *importXml;
    QAction *actionExport_from_XML;
    QAction *aboutSaving;
    QAction *exportXml;
    QWidget *centralWidget;
    QLabel *Label;
    QGroupBox *groupBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *name;
    QLabel *label_3;
    QLabel *type;
    QLabel *label_5;
    QLabel *eater;
    QLabel *label_7;
    QLabel *weight;
    QPushButton *era_menu;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *add_but;
    QPushButton *up_but;
    QPushButton *rem_but;
    QListWidget *listWidget;
    QLabel *picture;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QMenu *menuFile;
    QMenu *menuAbout;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(672, 653);
        exit = new QAction(MainWindow);
        exit->setObjectName(QString::fromUtf8("exit"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        exit->setFont(font);
        actionImport_from_XML = new QAction(MainWindow);
        actionImport_from_XML->setObjectName(QString::fromUtf8("actionImport_from_XML"));
        actionExport_to_XML = new QAction(MainWindow);
        actionExport_to_XML->setObjectName(QString::fromUtf8("actionExport_to_XML"));
        actionLog_out = new QAction(MainWindow);
        actionLog_out->setObjectName(QString::fromUtf8("actionLog_out"));
        importXml = new QAction(MainWindow);
        importXml->setObjectName(QString::fromUtf8("importXml"));
        actionExport_from_XML = new QAction(MainWindow);
        actionExport_from_XML->setObjectName(QString::fromUtf8("actionExport_from_XML"));
        aboutSaving = new QAction(MainWindow);
        aboutSaving->setObjectName(QString::fromUtf8("aboutSaving"));
        exportXml = new QAction(MainWindow);
        exportXml->setObjectName(QString::fromUtf8("exportXml"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        Label = new QLabel(centralWidget);
        Label->setObjectName(QString::fromUtf8("Label"));
        Label->setEnabled(false);
        Label->setGeometry(QRect(20, 320, 311, 41));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans"));
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        Label->setFont(font1);
        Label->setWordWrap(false);
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setEnabled(true);
        groupBox->setGeometry(QRect(350, 20, 301, 291));
        groupBox->setFont(font);
        formLayoutWidget = new QWidget(groupBox);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(0, 30, 301, 261));
        formLayoutWidget->setFont(font);
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setLabelAlignment(Qt::AlignCenter);
        formLayout->setFormAlignment(Qt::AlignCenter);
        formLayout->setHorizontalSpacing(10);
        formLayout->setVerticalSpacing(40);
        formLayout->setContentsMargins(30, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);
        label->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        name = new QLabel(formLayoutWidget);
        name->setObjectName(QString::fromUtf8("name"));
        name->setFont(font);
        name->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(0, QFormLayout::FieldRole, name);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);
        label_3->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        type = new QLabel(formLayoutWidget);
        type->setObjectName(QString::fromUtf8("type"));
        type->setFont(font);
        type->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(1, QFormLayout::FieldRole, type);

        label_5 = new QLabel(formLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);
        label_5->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_5);

        eater = new QLabel(formLayoutWidget);
        eater->setObjectName(QString::fromUtf8("eater"));
        eater->setFont(font);
        eater->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(2, QFormLayout::FieldRole, eater);

        label_7 = new QLabel(formLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);
        label_7->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_7);

        weight = new QLabel(formLayoutWidget);
        weight->setObjectName(QString::fromUtf8("weight"));
        weight->setFont(font);
        weight->setAutoFillBackground(false);
        weight->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(3, QFormLayout::FieldRole, weight);

        era_menu = new QPushButton(centralWidget);
        era_menu->setObjectName(QString::fromUtf8("era_menu"));
        era_menu->setGeometry(QRect(30, 520, 301, 41));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans"));
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setItalic(true);
        font2.setWeight(75);
        era_menu->setFont(font2);
        era_menu->setTabletTracking(false);
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(30, 380, 301, 121));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        add_but = new QPushButton(verticalLayoutWidget);
        add_but->setObjectName(QString::fromUtf8("add_but"));
        add_but->setEnabled(true);
        add_but->setFont(font);

        verticalLayout->addWidget(add_but);

        up_but = new QPushButton(verticalLayoutWidget);
        up_but->setObjectName(QString::fromUtf8("up_but"));
        up_but->setEnabled(false);
        up_but->setFont(font);

        verticalLayout->addWidget(up_but);

        rem_but = new QPushButton(verticalLayoutWidget);
        rem_but->setObjectName(QString::fromUtf8("rem_but"));
        rem_but->setEnabled(false);
        rem_but->setFont(font);

        verticalLayout->addWidget(rem_but);

        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(20, 20, 311, 301));
        listWidget->setFont(font);
        picture = new QLabel(centralWidget);
        picture->setObjectName(QString::fromUtf8("picture"));
        picture->setGeometry(QRect(363, 326, 281, 231));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 672, 28));
        menuBar->setFont(font);
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QString::fromUtf8("menuMenu"));
        menuMenu->setFont(font);
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuFile->setFont(font);
        menuAbout = new QMenu(menuBar);
        menuAbout->setObjectName(QString::fromUtf8("menuAbout"));
        menuAbout->setFont(font);
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuMenu->menuAction());
        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuAbout->menuAction());
        menuMenu->addSeparator();
        menuMenu->addAction(exit);
        menuMenu->addSeparator();
        menuMenu->addSeparator();
        menuMenu->addAction(actionLog_out);
        menuFile->addAction(importXml);
        menuFile->addAction(exportXml);
        menuAbout->addAction(aboutSaving);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Dinos Menu", nullptr));
        exit->setText(QApplication::translate("MainWindow", "Exit", nullptr));
#ifndef QT_NO_SHORTCUT
        exit->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", nullptr));
#endif // QT_NO_SHORTCUT
        actionImport_from_XML->setText(QApplication::translate("MainWindow", "Import from XML", nullptr));
        actionExport_to_XML->setText(QApplication::translate("MainWindow", "Export to XML", nullptr));
        actionLog_out->setText(QApplication::translate("MainWindow", "Log out", nullptr));
#ifndef QT_NO_SHORTCUT
        actionLog_out->setShortcut(QApplication::translate("MainWindow", "Ctrl+Z", nullptr));
#endif // QT_NO_SHORTCUT
        importXml->setText(QApplication::translate("MainWindow", "Import from XML", nullptr));
#ifndef QT_NO_SHORTCUT
        importXml->setShortcut(QApplication::translate("MainWindow", "Ctrl+R", nullptr));
#endif // QT_NO_SHORTCUT
        actionExport_from_XML->setText(QApplication::translate("MainWindow", "Export from XML", nullptr));
        aboutSaving->setText(QApplication::translate("MainWindow", "About saving data", nullptr));
        exportXml->setText(QApplication::translate("MainWindow", "Export to XML", nullptr));
#ifndef QT_NO_SHORTCUT
        exportXml->setShortcut(QApplication::translate("MainWindow", "Ctrl+W", nullptr));
#endif // QT_NO_SHORTCUT
        Label->setText(QApplication::translate("MainWindow", "~Double click to watch eras of dino", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Dino info", nullptr));
        label->setText(QApplication::translate("MainWindow", "Name", nullptr));
        name->setText(QApplication::translate("MainWindow", "name", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Type", nullptr));
        type->setText(QApplication::translate("MainWindow", "type", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "Eater", nullptr));
        eater->setText(QApplication::translate("MainWindow", "eater", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "Weight", nullptr));
        weight->setText(QApplication::translate("MainWindow", "weight", nullptr));
        era_menu->setText(QApplication::translate("MainWindow", "To Era menu -->", nullptr));
        add_but->setText(QApplication::translate("MainWindow", "Add", nullptr));
        up_but->setText(QApplication::translate("MainWindow", "Update", nullptr));
        rem_but->setText(QApplication::translate("MainWindow", "Remove", nullptr));
        picture->setText(QString());
        menuMenu->setTitle(QApplication::translate("MainWindow", "Account", nullptr));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", nullptr));
        menuAbout->setTitle(QApplication::translate("MainWindow", "About", nullptr));
        mainToolBar->setWindowTitle(QApplication::translate("MainWindow", "Dinos Menu", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
