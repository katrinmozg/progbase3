/********************************************************************************
** Form generated from reading UI file 'subaddupwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SUBADDUPWINDOW_H
#define UI_SUBADDUPWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SubAddUpWindow
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_5;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *nameEdit;
    QLineEdit *typeEdit;
    QLabel *label_8;
    QLabel *label_7;
    QLabel *label_6;
    QLabel *label_9;
    QListWidget *allListWidget;
    QListWidget *addedListWidget;

    void setupUi(QDialog *SubAddUpWindow)
    {
        if (SubAddUpWindow->objectName().isEmpty())
            SubAddUpWindow->setObjectName(QString::fromUtf8("SubAddUpWindow"));
        SubAddUpWindow->resize(487, 491);
        buttonBox = new QDialogButtonBox(SubAddUpWindow);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(90, 440, 341, 32));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        verticalLayoutWidget = new QWidget(SubAddUpWindow);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(20, 20, 98, 111));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        verticalLayout->addWidget(label_2);

        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        verticalLayout->addWidget(label);

        label_5 = new QLabel(SubAddUpWindow);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(20, 150, 441, 20));
        label_5->setFont(font);
        label_5->setAlignment(Qt::AlignCenter);
        verticalLayoutWidget_2 = new QWidget(SubAddUpWindow);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(140, 20, 321, 111));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        nameEdit = new QLineEdit(verticalLayoutWidget_2);
        nameEdit->setObjectName(QString::fromUtf8("nameEdit"));
        nameEdit->setFont(font);

        verticalLayout_2->addWidget(nameEdit);

        typeEdit = new QLineEdit(verticalLayoutWidget_2);
        typeEdit->setObjectName(QString::fromUtf8("typeEdit"));
        typeEdit->setFont(font);

        verticalLayout_2->addWidget(typeEdit);

        label_8 = new QLabel(SubAddUpWindow);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setEnabled(false);
        label_8->setGeometry(QRect(250, 190, 191, 31));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Noto Sans"));
        font1.setPointSize(11);
        font1.setBold(true);
        font1.setWeight(75);
        label_8->setFont(font1);
        label_8->setAlignment(Qt::AlignCenter);
        label_7 = new QLabel(SubAddUpWindow);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(200, 320, 61, 31));
        label_7->setFont(font);
        label_7->setAlignment(Qt::AlignCenter);
        label_6 = new QLabel(SubAddUpWindow);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(200, 280, 61, 31));
        label_6->setFont(font);
        label_6->setAlignment(Qt::AlignCenter);
        label_9 = new QLabel(SubAddUpWindow);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setEnabled(false);
        label_9->setGeometry(QRect(20, 190, 191, 31));
        label_9->setFont(font1);
        label_9->setAlignment(Qt::AlignCenter);
        allListWidget = new QListWidget(SubAddUpWindow);
        allListWidget->setObjectName(QString::fromUtf8("allListWidget"));
        allListWidget->setGeometry(QRect(30, 220, 171, 191));
        allListWidget->setFont(font);
        addedListWidget = new QListWidget(SubAddUpWindow);
        addedListWidget->setObjectName(QString::fromUtf8("addedListWidget"));
        addedListWidget->setGeometry(QRect(260, 220, 171, 192));
        addedListWidget->setFont(font);

        retranslateUi(SubAddUpWindow);
        QObject::connect(buttonBox, SIGNAL(accepted()), SubAddUpWindow, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SubAddUpWindow, SLOT(reject()));

        QMetaObject::connectSlotsByName(SubAddUpWindow);
    } // setupUi

    void retranslateUi(QDialog *SubAddUpWindow)
    {
        SubAddUpWindow->setWindowTitle(QApplication::translate("SubAddUpWindow", "Dialog", nullptr));
        label_2->setText(QApplication::translate("SubAddUpWindow", "Era", nullptr));
        label->setText(QApplication::translate("SubAddUpWindow", "Time period", nullptr));
        label_5->setText(QApplication::translate("SubAddUpWindow", "Chose connection with dino", nullptr));
        label_8->setText(QApplication::translate("SubAddUpWindow", "~Double click to remove", nullptr));
        label_7->setText(QApplication::translate("SubAddUpWindow", "<====", nullptr));
        label_6->setText(QApplication::translate("SubAddUpWindow", "====>", nullptr));
        label_9->setText(QApplication::translate("SubAddUpWindow", "~Double click to add", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SubAddUpWindow: public Ui_SubAddUpWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SUBADDUPWINDOW_H
