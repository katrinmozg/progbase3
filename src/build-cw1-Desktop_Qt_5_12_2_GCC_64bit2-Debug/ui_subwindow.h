/********************************************************************************
** Form generated from reading UI file 'subwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SUBWINDOW_H
#define UI_SUBWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SubWindow
{
public:
    QListWidget *listWidget;
    QGroupBox *groupBox;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *add_but;
    QPushButton *up_but;
    QPushButton *rem_but;
    QPushButton *back_to_Dino;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *era;
    QLabel *label_3;
    QLabel *time_period;
    QLabel *label_2;

    void setupUi(QDialog *SubWindow)
    {
        if (SubWindow->objectName().isEmpty())
            SubWindow->setObjectName(QString::fromUtf8("SubWindow"));
        SubWindow->resize(694, 495);
        listWidget = new QListWidget(SubWindow);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(20, 40, 311, 391));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        listWidget->setFont(font);
        groupBox = new QGroupBox(SubWindow);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(370, 30, 301, 171));
        groupBox->setFont(font);
        verticalLayoutWidget = new QWidget(SubWindow);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(370, 220, 301, 211));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        add_but = new QPushButton(verticalLayoutWidget);
        add_but->setObjectName(QString::fromUtf8("add_but"));
        add_but->setFont(font);

        verticalLayout->addWidget(add_but);

        up_but = new QPushButton(verticalLayoutWidget);
        up_but->setObjectName(QString::fromUtf8("up_but"));
        up_but->setFont(font);

        verticalLayout->addWidget(up_but);

        rem_but = new QPushButton(verticalLayoutWidget);
        rem_but->setObjectName(QString::fromUtf8("rem_but"));
        rem_but->setFont(font);

        verticalLayout->addWidget(rem_but);

        back_to_Dino = new QPushButton(SubWindow);
        back_to_Dino->setObjectName(QString::fromUtf8("back_to_Dino"));
        back_to_Dino->setGeometry(QRect(370, 450, 299, 31));
        back_to_Dino->setFont(font);
        formLayoutWidget = new QWidget(SubWindow);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(370, 60, 301, 141));
        formLayoutWidget->setFont(font);
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setLabelAlignment(Qt::AlignCenter);
        formLayout->setFormAlignment(Qt::AlignCenter);
        formLayout->setHorizontalSpacing(10);
        formLayout->setVerticalSpacing(40);
        formLayout->setContentsMargins(30, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);
        label->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        era = new QLabel(formLayoutWidget);
        era->setObjectName(QString::fromUtf8("era"));
        era->setFont(font);
        era->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(0, QFormLayout::FieldRole, era);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);
        label_3->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        time_period = new QLabel(formLayoutWidget);
        time_period->setObjectName(QString::fromUtf8("time_period"));
        time_period->setFont(font);
        time_period->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        formLayout->setWidget(1, QFormLayout::FieldRole, time_period);

        label_2 = new QLabel(SubWindow);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setEnabled(false);
        label_2->setGeometry(QRect(30, 440, 301, 41));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans"));
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        label_2->setFont(font1);
        label_2->setWordWrap(false);

        retranslateUi(SubWindow);

        QMetaObject::connectSlotsByName(SubWindow);
    } // setupUi

    void retranslateUi(QDialog *SubWindow)
    {
        SubWindow->setWindowTitle(QApplication::translate("SubWindow", "Era Menu", nullptr));
        groupBox->setTitle(QApplication::translate("SubWindow", "Era info", nullptr));
        add_but->setText(QApplication::translate("SubWindow", "Add", nullptr));
        up_but->setText(QApplication::translate("SubWindow", "Update", nullptr));
        rem_but->setText(QApplication::translate("SubWindow", "Remove", nullptr));
        back_to_Dino->setText(QApplication::translate("SubWindow", "<=Back to Dinos menu", nullptr));
        label->setText(QApplication::translate("SubWindow", "Era", nullptr));
        era->setText(QApplication::translate("SubWindow", "era", nullptr));
        label_3->setText(QApplication::translate("SubWindow", "Time period", nullptr));
        time_period->setText(QApplication::translate("SubWindow", "time period", nullptr));
        label_2->setText(QApplication::translate("SubWindow", "~Double click to watch dinos of era", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SubWindow: public Ui_SubWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SUBWINDOW_H
