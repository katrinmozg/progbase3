/********************************************************************************
** Form generated from reading UI file 'opendialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPENDIALOG_H
#define UI_OPENDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpenDialog
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *sig_but;
    QPushButton *log_but;
    QLabel *label;

    void setupUi(QDialog *OpenDialog)
    {
        if (OpenDialog->objectName().isEmpty())
            OpenDialog->setObjectName(QString::fromUtf8("OpenDialog"));
        OpenDialog->resize(400, 284);
        verticalLayoutWidget = new QWidget(OpenDialog);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(50, 90, 301, 141));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        sig_but = new QPushButton(verticalLayoutWidget);
        sig_but->setObjectName(QString::fromUtf8("sig_but"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        sig_but->setFont(font);

        verticalLayout->addWidget(sig_but);

        log_but = new QPushButton(verticalLayoutWidget);
        log_but->setObjectName(QString::fromUtf8("log_but"));
        log_but->setFont(font);

        verticalLayout->addWidget(log_but);

        label = new QLabel(OpenDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(50, 30, 301, 41));
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        retranslateUi(OpenDialog);

        QMetaObject::connectSlotsByName(OpenDialog);
    } // setupUi

    void retranslateUi(QDialog *OpenDialog)
    {
        OpenDialog->setWindowTitle(QApplication::translate("OpenDialog", "HELLO", nullptr));
        sig_but->setText(QApplication::translate("OpenDialog", "Sign up", nullptr));
        log_but->setText(QApplication::translate("OpenDialog", "Log in", nullptr));
        label->setText(QApplication::translate("OpenDialog", "Choose your action", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OpenDialog: public Ui_OpenDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPENDIALOG_H
