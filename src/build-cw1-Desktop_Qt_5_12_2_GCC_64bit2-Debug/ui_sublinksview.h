/********************************************************************************
** Form generated from reading UI file 'sublinksview.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SUBLINKSVIEW_H
#define UI_SUBLINKSVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_SubLinksView
{
public:
    QPushButton *pushButton;
    QListWidget *listWidget;

    void setupUi(QDialog *SubLinksView)
    {
        if (SubLinksView->objectName().isEmpty())
            SubLinksView->setObjectName(QString::fromUtf8("SubLinksView"));
        SubLinksView->resize(295, 344);
        pushButton = new QPushButton(SubLinksView);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(180, 300, 80, 25));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        pushButton->setFont(font);
        listWidget = new QListWidget(SubLinksView);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(20, 20, 256, 261));
        listWidget->setFont(font);

        retranslateUi(SubLinksView);

        QMetaObject::connectSlotsByName(SubLinksView);
    } // setupUi

    void retranslateUi(QDialog *SubLinksView)
    {
        SubLinksView->setWindowTitle(QApplication::translate("SubLinksView", "Dinos View", nullptr));
        pushButton->setText(QApplication::translate("SubLinksView", "<=Back", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SubLinksView: public Ui_SubLinksView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SUBLINKSVIEW_H
